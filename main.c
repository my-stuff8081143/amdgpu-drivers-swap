#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

int main()
{
   struct stat st;
   stat("/etc/default/grub", &st);
   size_t grub_file_size = st.st_size;

   FILE *grub_file = fopen("/etc/default/grub", "r+");
   char *grub_contents = malloc(grub_file_size);
   fread(grub_contents, grub_file_size, 1, grub_file);

   /* If radeon driver is being used */
   if (strstr(grub_contents, "radeon.si_support=1") != NULL && strstr(grub_contents, "amdgpu.si_support=0") != NULL)
   {
      for (int i = 0; i < grub_file_size; i++) {
         if (grub_contents[i] == 'r' && 
             grub_contents[i+1] == 'a' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'e' &&
             grub_contents[i+4] == 'o' &&
             grub_contents[i+5] == 'n' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 's' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == '_' &&
             grub_contents[i+10] == 's' &&
             grub_contents[i+11] == 'u' &&
             grub_contents[i+12] == 'p' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'o' &&
             grub_contents[i+15] == 'r' &&
             grub_contents[i+16] == 't' &&
             grub_contents[i+17] == '=' &&
             grub_contents[i+18] == '1') 
         {
            grub_contents[i+18] = '0';
         }

         if (grub_contents[i] == 'a' && 
             grub_contents[i+1] == 'm' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'g' &&
             grub_contents[i+4] == 'p' &&
             grub_contents[i+5] == 'u' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 's' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == '_' &&
             grub_contents[i+10] == 's' &&
             grub_contents[i+11] == 'u' &&
             grub_contents[i+12] == 'p' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'o' &&
             grub_contents[i+15] == 'r' &&
             grub_contents[i+16] == 't' &&
             grub_contents[i+17] == '=' &&
             grub_contents[i+18] == '0')
         {
            grub_contents[i+18] = '1';
            break;
         }
      }

      fclose(grub_file);
      FILE *grub_file_write = fopen("/etc/default/grub", "w");
      fwrite(grub_contents, grub_file_size, 1, grub_file_write);
      fclose(grub_file_write);
   }

   /* If amdgpu driver is being used */
   else if (strstr(grub_contents, "radeon.si_support=0") != NULL && strstr(grub_contents, "amdgpu.si_support=1") != NULL)
   {
      for (int i = 0; i < grub_file_size; i++) {
         if (grub_contents[i] == 'r' && 
             grub_contents[i+1] == 'a' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'e' &&
             grub_contents[i+4] == 'o' &&
             grub_contents[i+5] == 'n' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 's' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == '_' &&
             grub_contents[i+10] == 's' &&
             grub_contents[i+11] == 'u' &&
             grub_contents[i+12] == 'p' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'o' &&
             grub_contents[i+15] == 'r' &&
             grub_contents[i+16] == 't' &&
             grub_contents[i+17] == '=' &&
             grub_contents[i+18] == '0') 
         {
            grub_contents[i+18] = '1';
         }

         if (grub_contents[i] == 'a' && 
             grub_contents[i+1] == 'm' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'g' &&
             grub_contents[i+4] == 'p' &&
             grub_contents[i+5] == 'u' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 's' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == '_' &&
             grub_contents[i+10] == 's' &&
             grub_contents[i+11] == 'u' &&
             grub_contents[i+12] == 'p' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'o' &&
             grub_contents[i+15] == 'r' &&
             grub_contents[i+16] == 't' &&
             grub_contents[i+17] == '=' &&
             grub_contents[i+18] == '1')
         {
            grub_contents[i+18] = '0';
            break;
         }
      }
      
      fclose(grub_file);
      FILE *grub_file_write = fopen("/etc/default/grub", "w");
      fwrite(grub_contents, grub_file_size, 1, grub_file_write);
      fclose(grub_file_write);
   }
   
   else if (strstr(grub_contents, "radeon.cik_support=1") != NULL && strstr(grub_contents, "amdgpu.cik_support=0") != NULL)
   {
      for (int i = 0; i < grub_file_size; i++) {
         if (grub_contents[i] == 'r' && 
             grub_contents[i+1] == 'a' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'e' &&
             grub_contents[i+4] == 'o' &&
             grub_contents[i+5] == 'n' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 'c' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == 'k' &&
             grub_contents[i+10] == '_' &&
             grub_contents[i+11] == 's' &&
             grub_contents[i+12] == 'u' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'p' &&
             grub_contents[i+15] == 'o' &&
             grub_contents[i+16] == 'r' &&
             grub_contents[i+17] == 't' &&
             grub_contents[i+18] == '=' &&
             grub_contents[i+19] == '1')
         {
            grub_contents[i+19] = '0';
         }

         if (grub_contents[i] == 'a' && 
             grub_contents[i+1] == 'm' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'g' &&
             grub_contents[i+4] == 'p' &&
             grub_contents[i+5] == 'u' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 'c' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == 'k' &&
             grub_contents[i+10] == '_' &&
             grub_contents[i+11] == 's' &&
             grub_contents[i+12] == 'u' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'p' &&
             grub_contents[i+15] == 'o' &&
             grub_contents[i+16] == 'r' &&
             grub_contents[i+17] == 't' &&
             grub_contents[i+18] == '=' &&
             grub_contents[i+19] == '0')
         {
            grub_contents[i+19] = '1';
            break;
         }
      }

      fclose(grub_file);
      FILE *grub_file_write = fopen("/etc/default/grub", "w");
      fwrite(grub_contents, grub_file_size, 1, grub_file_write);
      fclose(grub_file_write);
   }
   
   else if (strstr(grub_contents, "radeon.cik_support=0") != NULL && strstr(grub_contents, "amdgpu.cik_support=1") != NULL)
   {
      for (int i = 0; i < grub_file_size; i++) {
         if (grub_contents[i] == 'r' && 
             grub_contents[i+1] == 'a' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'e' &&
             grub_contents[i+4] == 'o' &&
             grub_contents[i+5] == 'n' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 'c' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == 'k' &&
             grub_contents[i+10] == '_' &&
             grub_contents[i+11] == 's' &&
             grub_contents[i+12] == 'u' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'p' &&
             grub_contents[i+15] == 'o' &&
             grub_contents[i+16] == 'r' &&
             grub_contents[i+17] == 't' &&
             grub_contents[i+18] == '=' &&
             grub_contents[i+19] == '0')
         {
            grub_contents[i+19] = '1';
         }

         if (grub_contents[i] == 'a' && 
             grub_contents[i+1] == 'm' &&
             grub_contents[i+2] == 'd' &&
             grub_contents[i+3] == 'g' &&
             grub_contents[i+4] == 'p' &&
             grub_contents[i+5] == 'u' &&
             grub_contents[i+6] == '.' &&
             grub_contents[i+7] == 'c' &&
             grub_contents[i+8] == 'i' &&
             grub_contents[i+9] == 'k' &&
             grub_contents[i+10] == '_' &&
             grub_contents[i+11] == 's' &&
             grub_contents[i+12] == 'u' &&
             grub_contents[i+13] == 'p' &&
             grub_contents[i+14] == 'p' &&
             grub_contents[i+15] == 'o' &&
             grub_contents[i+16] == 'r' &&
             grub_contents[i+17] == 't' &&
             grub_contents[i+18] == '=' &&
             grub_contents[i+19] == '1')
         {
            grub_contents[i+19] = '0';
            break;
         }
      }

      fclose(grub_file);
      FILE *grub_file_write = fopen("/etc/default/grub", "w");
      fwrite(grub_contents, grub_file_size, 1, grub_file_write);
      fclose(grub_file_write);
   }

   /* Error Messages */
   else
   {
      if (strstr(grub_contents, "radeon") != NULL && strstr(grub_contents, "amdgpu") == NULL) {
         printf("ERROR!\n");
         printf("amdgpu PARAMETER WAS NOT FOUND IN /etc/default/grub!\n");
         
         free(grub_contents);
         return 2;
      }
      
      else if (strstr(grub_contents, "amdgpu") != NULL && strstr(grub_contents, "radeon") == NULL) {
         printf("ERROR!\n");
         printf("radeon PARAMETER WAS NOT FOUND IN /etc/default/grub!\n");

         free(grub_contents);
         return 3;
      }

      else if ((strstr(grub_contents, "radeon.si_support=1") != NULL && strstr(grub_contents, "amdgpu.si_support=1") != NULL) || (strstr(grub_contents, "radeon.cik_support=1") != NULL && strstr(grub_contents, "amdgpu.cik_support=1") != NULL)) {
         printf("ERROR!\n");
         printf("BOTH radeon AND amdgpu PARAMETERS ARE ENABLED!\n");
         printf("DISABLE ONE!\n");

         free(grub_contents);
         return 4;
      }

      else if ((strstr(grub_contents, "radeon.si_support=0") != NULL && strstr(grub_contents, "amdgpu.si_support=0") != NULL) || (strstr(grub_contents, "radeon.cik_support=0") != NULL && strstr(grub_contents, "amdgpu.cik_support=0") != NULL)) {
         printf("ERROR!\n");
         printf("BOTH radeon AND amdgpu PARAMETERS ARE DISABLED!\n");
         printf("ENABLE ONE!\n");

         free(grub_contents);
         return 5;
      }

      else if (strstr(grub_contents, "radeon") == NULL && strstr(grub_contents, "amdgpu") == NULL) {
         printf("ERROR!\n");
         printf("NEITHER radeon NOR amdgpu PARAMETERS WERE FOUND IN THE /etc/default/grub FILE!\n");

         free(grub_contents);
         return 6;
      }

      else {
         printf("ERROR!\n");
         printf("SOMETHING IS WRONG WITH YOUR /etc/default/grub FILE!\n");      

         free(grub_contents);
         return 1;
      }
   }

   /* Updates grub */
   free(grub_contents);
   system("grub-mkconfig -o /boot/grub/grub.cfg");
   execl("/bin/shutdown", "/bin/shutdown", "-r", "now");

   return 0;
}
